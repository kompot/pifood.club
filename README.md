# pifood.club

Pico CMS [sajt](https://pifood.kompot.si) z recepti iz [pif campa](https://pif.camp).

## Pico urejanje

[Osnove urejanja](https://picocms.org/docs/#creating-content)

## Markdown

[Basic Syntax](https://www.markdownguide.org/basic-syntax/) | [Extended Syntax](https://www.markdownguide.org/extended-syntax/) | [Hitra referenca - "plonklistek"](https://www.markdownguide.org/cheat-sheet/)