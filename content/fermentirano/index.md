---
Title: Fermentirano
Description: Primeri vsebine
Author: Jurij P
Date: 2021-10-04
Robots: noindex,nofollow
Template: index
---

# Primer slike

Tukaj:

![Še ena slika](%assets_url%/078_KAT9213_hr.jpg)

Slike se pa nahajajo v podfolderju `assets`, na istem nivoju kot `content`

