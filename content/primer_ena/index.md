---
Title: Navodila za urejanje, prvič
Description: Primeri vsebine
Author: Jurij P
Date: 2021-10-04
Robots: noindex,nofollow
Template: index
---

# Prva stran

Je vedno tista v folderju, ki se imenuje index.md.
Tako linkaš na [drugo stran](primer_ena/druga_stran) v tej kategoriji oz. tem folderju :)

Vsak fajl ima v začetku (opcionalne) metapodatke.

Če ni metapodatkov, se strani ne vidijo v meniju

